package rocks.homescreen;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2Session;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;
import net.minidev.json.JSONObject;
import org.apache.hc.client5.http.config.RequestConfig;
import org.apache.hc.client5.http.cookie.BasicCookieStore;
import org.apache.hc.client5.http.cookie.CookieStore;
import org.apache.hc.core5.http.HttpHeaders;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

public abstract class AbstractHttpClient {
    private final JSONRPC2Session rpcSession;
    private HttpHeaders globalHeaders;
    private CookieStore globalCookies;
    private RequestConfig globalConfig;

    protected String serverAddress;
    protected String basePath;
    protected int requestId = 1;

    protected final ObjectMapper mapper = new ObjectMapper();

    public AbstractHttpClient(String serverAddress, String basePath) throws MalformedURLException {
        this.serverAddress = serverAddress;
        this.basePath = basePath;
        this.globalConfig = RequestConfig.DEFAULT;
        this.globalCookies = new BasicCookieStore();

        var serverURL = new URL(this.serverAddress + this.basePath);
        this.rpcSession = new JSONRPC2Session(serverURL);
        this.rpcSession.getOptions().acceptCookies(true);
    }

//    public HttpHeaders getGlobalHeaders() {
//        return globalHeaders;
//    }
//
//    public CookieStore getGlobalCookies() {
//        return globalCookies;
//    }
//
//    public RequestConfig getGlobalConfig() {
//        return globalConfig;
//    }

    protected JSONObject post(String method, ObjectNode body) throws IOException, JSONRPC2SessionException {
        JSONRPC2Request request = new JSONRPC2Request(method, this.requestId++);
        Map<String, Object> result = mapper.convertValue(body, new TypeReference<Map<String, Object>>() {
        });
        request.setNamedParams(result);
        System.out.println(request.toString());
        var response = this.rpcSession.send(request);
        System.out.println(response.toString());
//        HttpPost request = new HttpPost(this.buildUri(uri));
//        ObjectMapper mapper = new ObjectMapper();
//        HttpEntity requestEntity = new StringEntity(mapper.writeValueAsString(body));
//        request.setEntity(requestEntity);
//        return this.httpClient.execute(request);
        return (JSONObject)response.getResult();
    }

    protected void setCustomHeaders(Map<String, String> headers) {
        var cc = new CustomHttpHeaderHandler(headers);
        this.rpcSession.setConnectionConfigurator(cc);
    }

//    protected String buildUri(String path) {
//        return this.serverAddress + this.basePath + path;
//    }
//
//    public void setGlobalHeaders(HttpHeaders globalHeaders) {
//        this.globalHeaders = globalHeaders;
//    }
}
