package rocks.homescreen;

import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;
import org.aeonbits.owner.ConfigFactory;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

public class Invoker {
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, JSONRPC2SessionException, InterruptedException {
        var invoker = new Invoker();
        invoker.run();
    }

    public void run() throws IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, JSONRPC2SessionException, InterruptedException {
        var cfg = ConfigFactory.create(KerioConfig.class);

        var client = new KerioUserHttpClient(cfg.kerioUrl());

        System.out.println(client.checkUserAccount(cfg.adminUser(), cfg.adminPassword()));

//        var client2 = new KerioHttpClient(
//                cfg.kerioUrl(),
//                "Kerio APIs Client Library for Java"
//        );
//
//        client2.login(cfg.adminUser(), cfg.adminPassword());
//
//        System.out.println(client2.userCount());

//        System.out.println(client2.findUserById("cca8278a-2dd0-e044-b579-08dc5b8fc99b"));
//        System.out.println(client2.findUserByUsername("tilli14@msn.com"));
//        System.out.println(client2.findUserByEmail("tilli14@msn.com"));

//        client.logout();
    }
}
