package rocks.homescreen;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class JsonRPCRequestMessage {
    private int id;
    private String version = "2.0";
    private String method;
    private ObjectNode params;

    public JsonRPCRequestMessage(int id, String method, ObjectNode params) {
        this.id = id;
        this.method = method;
        this.params = params;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public ObjectNode getParams() {
        return params;
    }

    public void setParams(ObjectNode params) {
        this.params = params;
    }
}
