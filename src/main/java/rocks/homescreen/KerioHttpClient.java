package rocks.homescreen;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;
import net.minidev.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

public class KerioHttpClient extends AbstractHttpClient {
    public static final String URI_LOGIN = "/";

    private String applicationName;
    private String applicationVendor;
    private String applicationVersion;

    public KerioHttpClient(String serverAddress, String applicationName) throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException, MalformedURLException {
        super(serverAddress, "/admin/api/jsonrpc");
        this.applicationName = applicationName;
        this.applicationVendor = "homescreen.rocks";
        this.applicationVersion = "1.0.0";
    }

    public void login(String username, String password) throws IOException, JSONRPC2SessionException {

        ObjectNode on = (ObjectNode) this.mapper.readTree(new File("./Login.json"));
        on
                .put("userName", username)
                .put("password", password)
                .with("application")
                .put("name", this.applicationName)
                .put("vendor", this.applicationVendor)
                .put("version", this.applicationVersion);

        var response = this.post("Session.login", on);
        System.out.println(response);
        System.out.println(response.get("token"));
        var token = response.get("token").toString();
        var headers = new HashMap<String, String>();
        headers.put("X-Token", token);
        this.setCustomHeaders(headers);
    }

    public void logout() throws IOException, JSONRPC2SessionException {
        var on = JsonNodeFactory.instance.objectNode();
        this.post("Session.logout", on);
    }

    public JSONObject createUser(String newUsername) throws IOException, JSONRPC2SessionException {
        ObjectNode on = (ObjectNode) this.mapper.readTree(new File("UserDataDefault.json"));

        // 1. create user
        ObjectNode on2 = (ObjectNode) this.mapper.readTree(new File("CreateUser.json"));
        on2
                .putArray("users")
                .add(on);

        return this.post("Users.create", on2);
    }

    public JSONObject findUserByUsername(String username) throws IOException, JSONRPC2SessionException {
        ObjectNode on = (ObjectNode) this.mapper.readTree(new File("FindUsers.json"));

        // selector
        on.with("query").withArray("fields")
                .add("id")
                .add("credentials")
                .add("fullName")
                .add("email")
                .add("groups")
        ;

        on.with("query").put("limit", 1);

        // filter
        var filterUsername = JsonNodeFactory.instance.objectNode();
        filterUsername.put("fieldName", "userName");
        filterUsername.put("comparator", "Eq");
        filterUsername.put("value", username);

        on.with("query")
                .putArray("conditions")
                .add(filterUsername);

        return this.post("Users.get", on);
    }

    public JSONObject findUserById(String id) throws IOException, JSONRPC2SessionException {
        ObjectNode on = (ObjectNode) this.mapper.readTree(new File("FindUsers.json"));

        // selector
        on.with("query").withArray("fields")
                .add("id")
                .add("credentials")
                .add("fullName")
                .add("email")
                .add("groups")
        ;
        on.with("query").put("limit", 1);
        // filter
        var filterUsername = JsonNodeFactory.instance.objectNode();
        filterUsername.put("fieldName", "id");
        filterUsername.put("comparator", "Eq");
        filterUsername.put("value", id);

        on.with("query")
                .putArray("conditions")
                .add(filterUsername);

        return this.post("Users.get", on);
    }

    public JSONObject findUserByEmail(String email) throws IOException, JSONRPC2SessionException {
        return findUserByEmail(email, null, null);
    }

    public JSONObject findUserByEmail(String email, Integer start, Integer limit) throws IOException, JSONRPC2SessionException {
        ObjectNode on = (ObjectNode) this.mapper.readTree(new File("FindUsers.json"));

        // selector
        on.with("query").withArray("fields")
                .add("id")
                .add("credentials")
                .add("fullName")
                .add("email")
                .add("groups")
        ;
        on.with("query").put("limit", 1);
        // filter
        var filterUsername = JsonNodeFactory.instance.objectNode();
        filterUsername.put("fieldName", "email");
        filterUsername.put("comparator", "Eq");
        filterUsername.put("value", email);

        on.with("query")
                .putArray("conditions")
                .add(filterUsername);

        if (start != null) {
            on.with("query").put("start", start);
        }
        if (limit != null) {
            on.with("query").put("limit", limit);
        }
        return this.post("Users.get", on);
    }

    public Long userCount() throws IOException, JSONRPC2SessionException {
        ObjectNode on = (ObjectNode) this.mapper.readTree(new File("FindUsers.json"));
        // selector
        on.with("query").withArray("fields")
                .add("id")
                .add("credentials")
                .add("fullName")
                .add("email")
                .add("groups")
        ;
        on.with("query").put("limit", 1);
        var response = this.post("Users.get", on);

        return (Long) response.get("totalItems");
    }

    public JSONObject getAllUsers() throws IOException, JSONRPC2SessionException {
        return getAllUsers(null, null);
    }

    public JSONObject getAllUsers(Integer start, Integer limit) throws IOException, JSONRPC2SessionException {
        ObjectNode on = (ObjectNode) this.mapper.readTree(new File("FindUsers.json"));
        // selector
        on.with("query").withArray("fields")
                .add("id")
                .add("credentials")
                .add("fullName")
                .add("email")
                .add("groups")
        ;
        if (start != null) {
            on.with("query").put("start", start);
        }
        if (limit != null) {
            on.with("query").put("limit", limit);
        }
        return this.post("Users.get", on);
    }
}
