package rocks.homescreen;

import java.io.IOException;
import java.net.CookieManager;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.stream.Collectors;

public class KerioUserHttpClient {
    public static final String URI_LOGIN = "/internal/dologin.php";

    private final String serverAddress;

    public KerioUserHttpClient(String serverAddress) {
        this.serverAddress = serverAddress;
    }

    public boolean checkUserAccount(String username, String password) throws IOException, InterruptedException {
        var client = HttpClient.newBuilder()
                .cookieHandler(new CookieManager())
                .followRedirects(HttpClient.Redirect.NORMAL)
                .build();

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("kerio_username", username);
        parameters.put("kerio_password", password);
        String formData = parameters.keySet().stream()
                .map(key -> key + "=" + URLEncoder.encode(parameters.get(key), StandardCharsets.UTF_8))
                .collect(Collectors.joining("&"));

        var request = HttpRequest.newBuilder()
                .uri(URI.create(this.serverAddress + URI_LOGIN))
                .headers("Content-Type", "application/x-www-form-urlencoded")
                .POST(HttpRequest.BodyPublishers.ofString(formData))
                .build();

        var response = client.send(request, HttpResponse.BodyHandlers.ofString());

        return "/".equals(response.request().uri().getPath());
    }
}
